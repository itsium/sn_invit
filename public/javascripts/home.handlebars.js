{{classname "App.views.Home"}} = (function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    var fx = new _ns.fx.Animatecss();

    function animate(self) {
        setTimeout(function() {
            $.addClass(self.blurEl, 'show-blur');
            setTimeout(function() {
                $.removeClass(self.title.el, 'hidden');
                $.removeClass(self.ring.el, 'hidden');
                fx.run({
                    fromEl: self.ring.el,
                    fromEffect: 'fadeIn',
                    speed: '500ms',
                    effect: 'bounceIn',
                    el: self.title.el,
                    callback: function() {

                        self.events.add('go-to-menu', {
                            xtype: 'events.click',
                            el: self.el,
                            autoAttach: true,
                            handler: function() {
                                _ns.History.go({{url "menu"}});
                            }
                        });

                    }
                });
            }, 1000);
        }, 1000);
    }

    return _parent.subclass({

        xtype: 'home',
        xtpl: 'container', // liste des ui dispo
        // cd roc/ && bin/roc list ionic.ui
        implements: ['events'],

        listeners: {
            afterrender: function(self) {
                // @TODO load image before anim
                self.blurEl = self.el.querySelector('.background-blur');
                self.on('show', function() {
                    animate(self);
                });
            }
        },

        constructor: function(opts) {

            opts = opts || {};

            opts.config = {
                cssClass: 'home'
            };

            var invitation = Data.invitation;

            var home = [{
                xtpl: 'container',
                config: {
                    cssClass: 'background'
                }
            }, {
                xtpl: 'container',
                config: {
                    cssClass: 'background-blur'
                }
            }, {
                xtpl: 'container',
                ref: 'title',
                refScope: this,
                config: {
                    cssClass: 'title hidden'
                },
                items: [
                    '<div class="names">',
                    invitation.groom,
                    ' & ',
                    invitation.bride,
                    '</div>'
                ].join('')
            }, {
                xtpl: 'container',
                ref: 'ring',
                refScope: this,
                config: {
                    cssClass: 'ring hidden'
                }
            }];

            opts.items = home;

            // opts.items = [{
            //     xtpl: 'content',
            //     config: {
            //         padding: false,
            //         style: 'height: 100%'
            //     },
            //     ref: 'content',
            //     refScope: this,
            //     items: home
            // }];

            _parent.prototype.constructor.call(this, opts);
        }
    });

}());