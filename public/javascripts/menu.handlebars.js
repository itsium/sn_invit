{{classname "App.views.Menu"}} = (function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    //var fx = new _ns.fx.Animatecss();

    return _parent.subclass({

        xtype: 'menu',
        xtpl: 'app-menu', // liste des ui dispo
        // cd roc/ && bin/roc list ionic.ui
        implements: ['events'],

        listeners: {
            afterrender: function(self) {
                // @TODO load image before anim
            }
        },

        constructor: function(opts) {

            opts = opts || {};

            var invitation = Data.invitation;

            opts.config = {
                cssClass: invitation.theme,
                groom: invitation.groom,
                bride: invitation.bride,
                weddingday: invitation.ldate,
                subdate: invitation.subdate,
                welcomemsg: invitation.welcomemsg
            };

            _parent.prototype.constructor.call(this, opts);
        }
    });

}());