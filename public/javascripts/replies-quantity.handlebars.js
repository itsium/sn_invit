(function() {

    var _effect,
        _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    function get_effect() {
        if (!_effect) {
            _effect = new (_ns.xtype('fx.animate.css'))();
        }
        return _effect;
    }

    function effect(el) {
        get_effect().run({
            queue: false,
            speed: '.3s',
            effect: 'rubberBand',
            //effect: 'pulse',
            el: el
        });
    }

    function buttons_icon(self, val) {
        if (val >= self.maxQuantity) {
            self.btnPlus.setIcon('ios7-plus-outline');
        } else {
            self.btnPlus.setIcon('ios7-plus');
        }

        if (val <= self.minQuantity) {
            self.btnMinus.setIcon('ios7-minus-outline');
        } else {
            self.btnMinus.setIcon('ios7-minus');
        }
    }

    function update_quantity(self, quantity) {
        self.quantity = parseInt(quantity);
        self.nbtickets.el.innerHTML = self.quantity;
        self.field.el.value = self.quantity;
        effect(self.nbtickets.el);
        buttons_icon(self, self.quantity);
    }

    _parent.subclass({

        xtype: 'replies-quantity',
        xtpl: 'container',
        hasListeners: ['quantity'],
        quantity: 1,
        defaultQuantity: 1,
        minQuantity: 1,
        maxQuantity: 10,

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            opts.config = {
                cssClass: 'cart-section'
            };

            opts.items = [{
                xtype: 'textfield',
                ref: 'field',
                refScope: self,
                config: {
                    type: 'hidden',
                    name: 'nbpeople',
                    value: '1',
                    required: true,
                    style: 'display: none;'
                }
            }, {
                xtpl: 'row',
                items: [{
                    xtype: 'button',
                    ref: 'btnMinus',
                    refScope: self,
                    config: {
                        cssClass: 'quantity-button',
                        colCssClass: 'text-right',
                        icon: 'ios7-minus-outline',
                        ui: ['all', 'clear', 'large']
                    },
                    handler: function() {

                        var val = Math.max(self.quantity - 1, self.minQuantity);

                        if (val !== self.quantity) {
                            update_quantity(self, val);
                        }
                    }
                }, {
                    xtpl: 'container',
                    ref: 'nbtickets',
                    refScope: self,
                    config: {
                        cssClass: 'quantity-field',
                        colCssClass: 'text-center'
                    },
                    items: self.quantity
                }, {
                    xtype: 'button',
                    ref: 'btnPlus',
                    refScope: self,
                    config: {
                        cssClass: 'quantity-button',
                        colCssClass: 'text-left',
                        icon: 'ios7-plus',
                        ui: ['all', 'clear', 'large']
                    },
                    handler: function() {

                        var val = Math.min(Math.max(self.quantity + 1, 1),
                                self.maxQuantity);

                        if (val !== self.quantity) {
                            update_quantity(self, val);
                        }
                    }
                }]
            }];

            _parent.prototype.constructor.call(self, opts);

        },

        setMax: function(max) {

            var self = this;

            self.maxQuantity = parseInt(max);
            if (self.quantity > self.maxQuantity) {
                update_quantity(self, self.maxQuantity);
            } else {
                update_quantity(self, self.quantity);
            }
        }

    });

}());