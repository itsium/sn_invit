{{classname "App.Settings"}} = new {{ns}}.Settings({
    serverUrl: window.server + 'app/1/',
    imageUrl: window.server,

    // music
    music: 'music',

    // events
    events: 'events.json',

    // reply
    replyAdd: 'replies/add.json?name={name}&nbpeople={nbpeople}&phone={phone}&message={message}&is_diner_guest={event_type}',

    // blessings
    blessings: 'blessings.json',
    blessingAdd: 'blessings/add.json?name={name}&content={message}'
});