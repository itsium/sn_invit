{{classname "App.home.Router"}} = new (function() {

    var _session,
        _history,
        _ns = {{ns}},
        _parent = _ns.RouterView;

    // _ns.global.on('ready', function() {
    //     _history = _ns.History;
    //     _loader = App.Loadmask;
    // });

    var invitation = Data.invitation;

    if (invitation.music && invitation.music.length > 0) {

        var music = invitation.music;

        if (music.substr(0, 1) === '/') {
            music = music.substr(1);
        }

        document.body.insertAdjacentHTML('beforeend', [
            '<audio preload="auto" id="audio_play" loop="loop"><source src="',
                window.server, music,
                '" type="audio/mpeg"></audio>'
        ].join(''));
        document.body.onclick = function() {
            var musicp = document.getElementById("audio_play");
            musicp.play();
        };
    }

    return _parent.subclass({

        className: 'App.home.Router',
        xtype: 'home.router',

        routes: {
            '': 'routeDefault',
            {{ route "/home" as="home" }}: 'routeDefault',
            {{ route "/menu" as="menu" }}: 'routeMenu',
            {{ route "/events" as="events" }}: 'routeEvents',
            {{ route "/replies" as="replies" }}: 'routeReplies',
            {{ route "/blessings" as="blessings" }}: 'routeBlessings',
            {{ route "/album" as="album" }}: 'routeAlbum'
        },
        routeNoMatch: 'routeDefault',
        // fx: {
        //     'menu': {
        //         xtype: 'fx.animate.css',
        //         show: {
        //             speed: '1s',
        //             me: 'bounceIn',
        //             other: 'slideOutUp'
        //         }
        //     }
        // },

        routeDefault: function() {
            this.setCurrentView('home');
            document.title = 'HOME';
        },

        routeMenu: function() {
            this.setCurrentView('menu');
            document.title = 'MENU';
        },

        routeEvents: function() {
            this.setCurrentView('events');
            document.title = 'EVENTS';
        },

        routeReplies: function() {
            this.setCurrentView('replies');
            document.title = 'REPLIES';
        },

        routeBlessings: function() {
            this.setCurrentView('blessings');
            document.title = 'BLESSINGS';
        },

        routeAlbum: function() {
            this.setCurrentView('album');
            document.title = 'ALBUM';
        }

    });

}())();