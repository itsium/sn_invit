{{classname "App.views.Replies"}} = (function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    //var fx = new _ns.fx.Animatecss();

    return _parent.subclass({

        xtype: 'replies',
        xtpl: 'container', // liste des ui dispo
        // cd roc/ && bin/roc list ionic.ui
        implements: ['events'],

        listeners: {
            afterrender: function(self) {
                self.scroll = new _ns.physics.Scroll({
                    el: self.content.el,
                    cmp: self,
                    scrollable: 'y'
                });
                self.scroll.resize();
                self.content.on('aftersubmit', function(form, success, data) {

                    if (success && data.success) {
                        alert('Thank you !');
                        _ns.History.go({{url "menu"}});
                    } else {
                        alert('Sorry somethings went wrong. Please try again later.');
                    }

                });
            }
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            opts.config = {
                cssClass: 'page-replies ' + Data.invitation.theme
            };

            var image = 'images/replies/background.jpg';

            // words:

            var items = ['<div class="image" style="background-image: url(' + image + ');"></div>',{
                xtype: 'textfield',
                config: {
                    style: 'border-top-color: transparent;',
                    label: '姓名',
                    name: 'name',
                    required: true
                }
            }, '<span class="item item-input" style="height: 47px;"><span class="input-label">人数</span>', {
                xtype: 'replies-quantity'
                // xtype: 'textfield',
                // config: {
                //     label: '人数',
                //     name: 'nb_people',
                //     required: true
                // }
            }, '</span><input type="hidden" name="event_type" value="' + (Data.event_type === "s") + '" />', {
                xtype: 'textfield',
                config: {
                    label: '手机号码',
                    name: 'phone',
                    required: true
                }
            }, {
                xtype: 'textarea',
                config: {
                    label: '留言',
                    name: 'message',
                    style: 'height: 75px; border-bottom-color: transparent;',
                    required: false
                }
            }, {
                xtype: 'button',
                config: {
                    text: '回复',
                    style: 'margin: 10px 4%; width: 92%;',
                    ui: ['block', 'brown']
                },
                handler: function() {
                    self.content.submit();
                }
            }];

            opts.items = [{
                xtype: 'forms.simple',
                xtpl: 'content',
                ref: 'content',
                refScope: self,
                loadmask: App.Loadmask,
                url: App.Settings.gets('serverUrl', 'replyAdd').join(''),
                config: {
                    padding: false
                },
                items: items
            }];

            _parent.prototype.constructor.call(self, opts);
        }
    });

}());