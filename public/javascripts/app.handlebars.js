(function() {

    var _ns = {{ns}};

    window.onerror = function() {
        alert(JSON.stringify(arguments));
    };

    var app = new _ns.App({
        engine: 'ionic',
        onready: function() {

            var loadmask = App.Loadmask = new _ns.views.Loadmask({
                    lock: true
                });

            loadmask.compile();
            loadmask.render('body');
            loadmask.hide();

        }
    });

}());
