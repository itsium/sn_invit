{{classname "App.views.Blessings"}} = (function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    //var fx = new _ns.fx.Animatecss();

    return _parent.subclass({

        xtype: 'blessings',
        xtpl: 'container', // liste des ui dispo
        // cd roc/ && bin/roc list ionic.ui
        implements: ['events'],

        listeners: {
            afterrender: function(self) {
                self.scroll = new _ns.physics.Scroll({
                    autoResize: true,
                    el: self.content.el,
                    cmp: self,
                    scrollable: 'y',
                    pullToRefresh: true,
                    onrefresh: function(done) {
                        self.done = done;
                        self.list.reload();
                    },
                    afterscroll: function() {

                        var sizes = self.scroll.getSizes();

                        if ((sizes.top + 100) >= sizes.height) {
                            self.list.loadNext();
                        }
                    }
                });
                self.events.add('focus-name', {
                    xtype: 'events.click',
                    el: self.name.el,
                    autoAttach: true,
                    handler: function() {
                        if ($.hasClass(self.form.el, 'show') === false) {
                            $.addClass(self.form.el, 'show');
                            self.name.el.focus();
                        }
                    }
                });
                self.form.on('aftersubmit', function(form, success, data) {
                    if (success && data.success) {
                        self.form.reset();
                        $.removeClass(self.form.el, 'show');
                        self.list.reload();
                    } else {
                        alert('Sorry, something went wrong. Please try again later.');
                    }
                });
            }
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            opts.config = {
                cssClass: 'page-blessings ' + Data.invitation.theme
            };

            // words:

            self.store = new _ns.data.Pagingstore({
                enableHashTable: true,
                recordPerPage: 5,
                //storageCache: true,
                proxy: {
                    type: 'jsonp',
                    cache: false,
                    url: App.Settings.gets('serverUrl', 'blessings').join('')
                }
            });

            self.store.on('afterload', function() {
                if (self.done) {
                    self.done();
                    delete self.done;
                }
            });

            var items = [{
                xtype: 'listbuffered',
                xtpl: 'blessings',
                itemTpl: 'blessing-item',
                ref: 'list',
                refScope: self,
                store: self.store,
                emptyTpl: '<div class="empty">还没人祝福这对新人，送出第一个祝福。</div>',
                items: []
            }];

            opts.items = [{
                xtpl: 'content',
                ref: 'content',
                refScope: self,
                config: {
                    padding: false
                },
                items: items
            }, {
                xtype: 'forms.simple',
                xtpl: 'content',
                config: {
                    cssClass: 'form'
                },
                ref: 'form',
                refScope: self,
                loader: App.Loadmask,
                url: App.Settings.gets('serverUrl', 'blessingAdd').join(''),
                items: [{
                    xtype: 'textfield',
                    ref: 'name',
                    refScope: self,
                    config: {
                        cssClass: 'name',
                        name: 'name',
                        placeHolder: '名字',
                        required: true
                    }
                }, {
                    xtype: 'textarea',
                    config: {
                        cssClass: 'message',
                        placeHolder: '你的祝福...',
                        name: 'message',
                        style: 'height: 75px;padding-top:10px;',
                        required: true
                    }
                }, '<div class="row"><div class="col col-75" style="padding: 0 5px 0 0;">', {
                    xtype: 'button',
                    config: {
                        text: 'Send',
                        ui: ['block', 'brown']
                    },
                    handler: function() {
                        self.form.submit();
                    }
                }, '</div><div class="col" style="padding: 0 0 0 5px;">', {
                    xtype: 'button',
                    config: {
                        style: 'color: #666;',
                        icon: 'ios7-arrow-down',
                        ui: ['block', 'clear']
                    },
                    handler: function() {
                        $.removeClass(self.form.el, 'show');
                    }
                }, '</div></div>']
            }, '<div class="title-background"></div><div class="title-top"></div>'];
            //<div class="bottom-background"></div>'];

            _parent.prototype.constructor.call(self, opts);
        }
    });

}());