{{classname "App.views.Album"}} = (function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _handlebars = Handlebars,
        _parent = _ns.views.Template;

    _handlebars.registerHelper('gallery_image', function(context, options) {

        //if (options.data.index) first last

        var result = '',
            loop = options.data;

        if (!(loop.index % 3)) {
            if (loop.first === false) {
                result += '</div>';
            }
            result += '<div class="gallery-row">';
        }

        var server = options.hash.server;

        if (server.substr(-1) === '/') {
            server = server.substr(0, server.lastIndexOf('/'));
        }

        if (!server.length) {
            server = 'http://' + window.location.host;
        }

        result += [
            '<div class="gallery-item">',
                '<a href="javascript:;" data-image="', server, this.image, '" style="',
                    'background-image: url(', server, this.thumb, ');">',
                    '&nbsp;',
                '</a>',
            '</div>'
        ].join('');

        if (loop.last === true) {
            result += '</div>';
        }

        return (new _handlebars.SafeString(result));

    });

    return _parent.subclass({

        xtype: 'album',
        xtpl: 'container',

        listeners: {
            afterrender: function(self) {

                self.scroll = new _ns.physics.Scroll({
                    autoResize: true,
                    el: self.content.el,
                    cmp: self,
                    scrollable: 'y'
                });

                var photoSwipe = Code.PhotoSwipe;

                var GetImageSource = function(el){
                    return el.getAttribute('data-image');
                };

                self.gallery = photoSwipe.attach(
                    self.content.el.querySelectorAll('a'), {
                        getImageSource: GetImageSource
                    }
                );
                self.scroll.resize();

                self.renderer.on('show', function() {
                    if (!self.gallery) {
                        self.gallery = photoSwipe.attach(
                            self.content.el.querySelectorAll('a'), {
                                getImageSource: GetImageSource
                            }
                        );
                        self.scroll.resize();
                    }
                });

                self.renderer.on('hide', function() {
                    if (self.gallery) {
                        photoSwipe.detatch(self.gallery);
                        self.gallery = undefined;
                    }
                });

            }
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            var data = Data;

            opts.config = {
                cssClass: 'page-album ' + data.invitation.theme
            };

            self.length = data.gallery.length;

            opts.items = [{
                xtpl: 'content',
                ref: 'content',
                refScope: self,
                config: {
                    padding: false
                },
                items: {
                    xtpl: 'app-album',
                    config: {
                        cssClass: 'gallery',
                        style: 'height: 100%;',
                        server: App.Settings.get('imageUrl'),
                        images: data.gallery
                    }
                }
            }];

            _parent.prototype.constructor.call(self, opts);
        }
    });

}());