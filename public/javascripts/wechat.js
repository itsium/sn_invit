(function () {

    function Q(a) {
        if (!a) {
            return null
        }
        if (a.constructor == String) {
            var b = document.querySelectorAll(a);
            return b
        }
        return a
    }

    window.onerror = function () {
        alert(JSON.stringify(arguments));
    };

    var weixin = window.WeixinJSBridge,
        eventName = 'WeixinJSBridgeReady';

    // var openAppController=function(h){

    //     return{
    //         callDone:false,showDone:false,init:function(){
    //             if(typeof weixin!="undefined"&&weixin.invoke){
    //                 openAppController.hasNewsApp()
    //             }
    //             else{
    //                 document.addEventListener(eventName,openAppController.hasNewsApp)
    //             }
    //             openAppController.reCheck()
    //         }
    //         ,reCheck:function(){
    //             setTimeout(function(){
    //                 openAppController.showDownload()
    //             }
    //             ,6000)
    //         }

    //     }
    // }(window);



    // var showMoreNews={
    //     isBeginHide:false,init:function(){
    //         var b=Q("#content .text, #content .preLoad");
    //         for(var a=0,g=b.length;a<g;a++){
    //             var f=b[a];
    //             var d=f.innerHTML;
    //             if(showMoreNews.isBeginHide){
    //                 Q.addClass(f,"showMoreHide")
    //             }
    //             if(/\$\$\$/i.test(d)){
    //                 f.innerHTML=d.replace(/<\!\-\-\$\$\$\-\->|\$\$\$/ig,"");
    //                 showMoreNews.createMoreHTML(f);
    //                 showMoreNews.isBeginHide=true
    //             }
    //         }
    //     }

    // };

    var bindShareWithApp = function () {
        var a = document.location.href;
        weixin.on("menu:share:appmessage", function (b) {
            weixin.invoke("sendAppMessage", {
                appid: "",
                img_url: contentModel.img_url,
                img_width: "65",
                img_height: "65",
                link: a,
                desc: contentModel.src,
                title: contentModel.title
            }, function (d) {
                weixin.log(d.err_msg)
            })
        })
    };

    var bindShareWithTimeline = function () {
        var a = document.location.href;
        weixin.on("menu:share:timeline", function (b) {
            weixin.invoke("shareTimeline", {
                img_url: contentModel.img_url,
                img_width: "65",
                img_height: "65",
                link: a,
                desc: "view.inews.qq.com",
                title: contentModel.title
            }, function (d) {
                weixin.log(d.err_msg)
            })
        })
    };

    var weChatBridgeReady = {
        init: function () {
            if (!weixin) weixin = window.WeixinJSBridge;
            weChatBridgeReady.bindShareWithApp();
            weChatBridgeReady.bindShareWithTimeline();
            var f = Q("img");
            for (var b = 0, d = f.length; b < d; b++) {
                var a = f[b];
                Q.tap(a, weChatBridgeReady.clickHandler)
            }
        },
        bindShareWithApp: bindShareWithApp,
        bindShareWithTimeline: bindShareWithTimeline
    };

    //showMoreNews.init();
    //openAppController.init();
    if (typeof weixin != "undefined" && weixin.invoke) {
        weChatBridgeReady.init()
    } else {
        document.addEventListener(eventName, weChatBridgeReady.init)
    }
    //downloadClick.init();

}());