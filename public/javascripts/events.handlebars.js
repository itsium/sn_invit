{{classname "App.views.Events"}} = (function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    //var fx = new _ns.fx.Animatecss();

    return _parent.subclass({

        xtype: 'events',
        xtpl: 'container', // liste des ui dispo
        // cd roc/ && bin/roc list ionic.ui
        implements: ['events'],

        listeners: {
            afterrender: function(self) {
                self.scroll = new _ns.physics.Scroll({
                    el: self.content.el,
                    cmp: self,
                    scrollable: 'y'
                });
                self.scroll.resize();
            }
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            opts.config = {
                cssClass: 'page-events ' + Data.invitation.theme
            };

            var items = [];

            Data.events.forEach(function(evt) {
                items.push({
                    xtpl: 'event-item',
                    config: {
                        event: evt
                    },
                    items: [{
                        xtype: 'maps.google',
                        mapOptions: {
                            disableDefaultUI: true,
                            scrollwheel: false,
                            navigationControl: false,
                            draggable: false,
                            zoom: 16
                        },
                        markers: [evt]
                    }]
                });
            });

            opts.items = [{
                xtpl: 'content',
                ref: 'content',
                refScope: self,
                config: {
                    padding: true
                },
                items: items
            }];

            _parent.prototype.constructor.call(self, opts);
        }
    });

}());