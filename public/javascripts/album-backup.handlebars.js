{{classname "App.views.Album"}} = (function() {

    var _ns = {{ns}},
        $ = _ns.Selector,
        _parent = _ns.views.Template;

    //var fx = new _ns.fx.Animatecss();

    return _parent.subclass({

        xtype: 'album',
        xtpl: 'container',

        listeners: {
            afterrender: function(self) {

                var width = window.innerWidth,
                    height = window.innerHeight;

                self.content.el.style.width = (self.length * width) + 'px';
                self.content.el.style.height = height + 'px';

                var pics = self.content.el.querySelectorAll('.photo');

                [].forEach.call(pics, function(pic) {
                    pic.style.width = width + 'px';
                    pic.style.height = height + 'px';
                });

                self.scroll = new _ns.physics.Scroll({
                    autoResize: true,
                    el: self.content.el,
                    cmp: self,
                    scrollable: 'x',
                    // zoomable: true,
                    // minZoom: 1,
                    // maxZoom: 2.5,
                    //snapping: true,
                    paging: true
                });
            }
        },

        constructor: function(opts) {

            var self = this;

            opts = opts || {};

            var data = Data;

            opts.config = {
                cssClass: 'page-album ' + data.invitation.theme
            };

            self.length = data.gallery.length;

            opts.items = [{
                xtpl: 'app-album',
                ref: 'content',
                refScope: self,
                config: {
                    style: 'height: 100%;',
                    server: App.Settings.get('imageUrl'),
                    images: data.gallery
                }
            }];

            _parent.prototype.constructor.call(self, opts);
        }
    });

}());