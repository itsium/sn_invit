require 'spec_helper'

describe "blessings/edit" do
  before(:each) do
    @blessing = assign(:blessing, stub_model(Blessing,
      :content => "MyText",
      :name => "MyString",
      :invitation => nil
    ))
  end

  it "renders the edit blessing form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", blessing_path(@blessing), "post" do
      assert_select "textarea#blessing_content[name=?]", "blessing[content]"
      assert_select "input#blessing_name[name=?]", "blessing[name]"
      assert_select "input#blessing_invitation[name=?]", "blessing[invitation]"
    end
  end
end
