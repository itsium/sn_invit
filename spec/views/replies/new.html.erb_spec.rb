require 'spec_helper'

describe "replies/new" do
  before(:each) do
    assign(:reply, stub_model(Reply,
      :name => "MyString",
      :nbpeople => "MyString",
      :phone => "MyString",
      :message => "MyString",
      :invitation => nil
    ).as_new_record)
  end

  it "renders new reply form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", replies_path, "post" do
      assert_select "input#reply_name[name=?]", "reply[name]"
      assert_select "input#reply_nbpeople[name=?]", "reply[nbpeople]"
      assert_select "input#reply_phone[name=?]", "reply[phone]"
      assert_select "input#reply_message[name=?]", "reply[message]"
      assert_select "input#reply_invitation[name=?]", "reply[invitation]"
    end
  end
end
