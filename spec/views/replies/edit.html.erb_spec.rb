require 'spec_helper'

describe "replies/edit" do
  before(:each) do
    @reply = assign(:reply, stub_model(Reply,
      :name => "MyString",
      :nbpeople => "MyString",
      :phone => "MyString",
      :message => "MyString",
      :invitation => nil
    ))
  end

  it "renders the edit reply form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", reply_path(@reply), "post" do
      assert_select "input#reply_name[name=?]", "reply[name]"
      assert_select "input#reply_nbpeople[name=?]", "reply[nbpeople]"
      assert_select "input#reply_phone[name=?]", "reply[phone]"
      assert_select "input#reply_message[name=?]", "reply[message]"
      assert_select "input#reply_invitation[name=?]", "reply[invitation]"
    end
  end
end
