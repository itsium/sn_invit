require 'spec_helper'

describe "replies/index" do
  before(:each) do
    assign(:replies, [
      stub_model(Reply,
        :name => "Name",
        :nbpeople => "Nbpeople",
        :phone => "Phone",
        :message => "Message",
        :invitation => nil
      ),
      stub_model(Reply,
        :name => "Name",
        :nbpeople => "Nbpeople",
        :phone => "Phone",
        :message => "Message",
        :invitation => nil
      )
    ])
  end

  it "renders a list of replies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Nbpeople".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Message".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
