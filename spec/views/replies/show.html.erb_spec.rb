require 'spec_helper'

describe "replies/show" do
  before(:each) do
    @reply = assign(:reply, stub_model(Reply,
      :name => "Name",
      :nbpeople => "Nbpeople",
      :phone => "Phone",
      :message => "Message",
      :invitation => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Nbpeople/)
    rendered.should match(/Phone/)
    rendered.should match(/Message/)
    rendered.should match(//)
  end
end
