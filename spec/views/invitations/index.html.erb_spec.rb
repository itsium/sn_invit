require 'spec_helper'

describe "invitations/index" do
  before(:each) do
    assign(:invitations, [
      stub_model(Invitation,
        :bride => "Bride",
        :groom => "Groom",
        :theme => "Theme",
        :user => nil
      ),
      stub_model(Invitation,
        :bride => "Bride",
        :groom => "Groom",
        :theme => "Theme",
        :user => nil
      )
    ])
  end

  it "renders a list of invitations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Bride".to_s, :count => 2
    assert_select "tr>td", :text => "Groom".to_s, :count => 2
    assert_select "tr>td", :text => "Theme".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
