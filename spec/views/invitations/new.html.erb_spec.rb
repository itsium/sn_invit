require 'spec_helper'

describe "invitations/new" do
  before(:each) do
    assign(:invitation, stub_model(Invitation,
      :bride => "MyString",
      :groom => "MyString",
      :theme => "MyString",
      :user => nil
    ).as_new_record)
  end

  it "renders new invitation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", invitations_path, "post" do
      assert_select "input#invitation_bride[name=?]", "invitation[bride]"
      assert_select "input#invitation_groom[name=?]", "invitation[groom]"
      assert_select "input#invitation_theme[name=?]", "invitation[theme]"
      assert_select "input#invitation_user[name=?]", "invitation[user]"
    end
  end
end
