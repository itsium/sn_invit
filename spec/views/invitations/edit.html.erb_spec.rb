require 'spec_helper'

describe "invitations/edit" do
  before(:each) do
    @invitation = assign(:invitation, stub_model(Invitation,
      :bride => "MyString",
      :groom => "MyString",
      :theme => "MyString",
      :user => nil
    ))
  end

  it "renders the edit invitation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", invitation_path(@invitation), "post" do
      assert_select "input#invitation_bride[name=?]", "invitation[bride]"
      assert_select "input#invitation_groom[name=?]", "invitation[groom]"
      assert_select "input#invitation_theme[name=?]", "invitation[theme]"
      assert_select "input#invitation_user[name=?]", "invitation[user]"
    end
  end
end
