require 'spec_helper'

describe "events/edit" do
  before(:each) do
    @event = assign(:event, stub_model(Event,
      :name => "MyString",
      :address => "MyText",
      :longitude => 1.5,
      :latitude => 1.5,
      :phone => "MyString",
      :invitation => nil
    ))
  end

  it "renders the edit event form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", event_path(@event), "post" do
      assert_select "input#event_name[name=?]", "event[name]"
      assert_select "textarea#event_address[name=?]", "event[address]"
      assert_select "input#event_longitude[name=?]", "event[longitude]"
      assert_select "input#event_latitude[name=?]", "event[latitude]"
      assert_select "input#event_phone[name=?]", "event[phone]"
      assert_select "input#event_invitation[name=?]", "event[invitation]"
    end
  end
end
