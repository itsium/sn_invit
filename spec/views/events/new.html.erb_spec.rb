require 'spec_helper'

describe "events/new" do
  before(:each) do
    assign(:event, stub_model(Event,
      :name => "MyString",
      :address => "MyText",
      :longitude => 1.5,
      :latitude => 1.5,
      :phone => "MyString",
      :invitation => nil
    ).as_new_record)
  end

  it "renders new event form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", events_path, "post" do
      assert_select "input#event_name[name=?]", "event[name]"
      assert_select "textarea#event_address[name=?]", "event[address]"
      assert_select "input#event_longitude[name=?]", "event[longitude]"
      assert_select "input#event_latitude[name=?]", "event[latitude]"
      assert_select "input#event_phone[name=?]", "event[phone]"
      assert_select "input#event_invitation[name=?]", "event[invitation]"
    end
  end
end
