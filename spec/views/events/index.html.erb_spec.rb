require 'spec_helper'

describe "events/index" do
  before(:each) do
    assign(:events, [
      stub_model(Event,
        :name => "Name",
        :address => "MyText",
        :longitude => 1.5,
        :latitude => 1.5,
        :phone => "Phone",
        :invitation => nil
      ),
      stub_model(Event,
        :name => "Name",
        :address => "MyText",
        :longitude => 1.5,
        :latitude => 1.5,
        :phone => "Phone",
        :invitation => nil
      )
    ])
  end

  it "renders a list of events" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
