# == Schema Information
#
# Table name: invitations
#
#  id                   :integer          not null, primary key
#  bride                :string(255)
#  groom                :string(255)
#  weddingday           :date
#  theme                :string(255)
#  user_id              :integer
#  created_at           :datetime
#  updated_at           :datetime
#  name                 :string(255)
#  welcomemsg           :string(255)
#  bgmusic_file_name    :string(255)
#  bgmusic_content_type :string(255)
#  bgmusic_file_size    :integer
#  bgmusic_updated_at   :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invitation do
    bride "MyString"
    groom "MyString"
    weddingday "2014-04-17"
    theme "MyString"
    user nil
  end
end
