# == Schema Information
#
# Table name: replies
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  nbpeople      :string(255)
#  phone         :string(255)
#  message       :string(255)
#  invitation_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :reply do
    name "MyString"
    nbpeople "MyString"
    phone "MyString"
    message "MyString"
    invitation nil
  end
end
