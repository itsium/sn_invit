# == Schema Information
#
# Table name: blessings
#
#  id            :integer          not null, primary key
#  content       :text
#  name          :string(255)
#  invitation_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :blessing do
    content "MyText"
    name "MyString"
    invitation nil
  end
end
