# == Schema Information
#
# Table name: events
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  address       :text
#  longitude     :float
#  latitude      :float
#  eventtime     :datetime
#  phone         :string(255)
#  invitation_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    name "MyString"
    address "MyText"
    longitude 1.5
    latitude 1.5
    eventtime "2014-04-17 23:25:41"
    phone "MyString"
    invitation nil
  end
end
