SnInvit::Application.routes.draw do
  resources :replies do
    collection do
      get "myreplies/:id" => "replies#index", as: :myreplies
    end
  end
  resources :blessings do
    collection do
      get "myblessings/:id" => "blessings#index", as: :myblessings
    end
  end

  resources :events do
    collection do
      get ":id" => "events#index", as: :myevents
      get "destroy_event/:id" => "events#destroy_event", as: "destroy_event"
    end
  end


  resources :invitations do
    collection do
      get "galery/:id" => "invitations#galery", as: :myphotos

      post "add_attachement/:id" => "invitations#add_attachement", as: :add_attachement
      get "destroy_attachement/:id" => "invitations#destroy_attachement", as: :destroy_attachement
    end
  end

  # <app>
  get 'app/:invitation_id/:event_type/initialize' => 'home#app'
  get 'app/:invitation_id/blessings' => 'blessings#app_list'
  get 'app/:invitation_id/blessings/add' => 'blessings#app_add'
  get 'app/:invitation_id/replies/add' => 'replies#app_add'
  get 'app/:invitation_id/music' => 'home#music'
  # </app>

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # get "home/index"
  devise_for :users
  get "dashboard" => 'users#show', as: "user"
  get "settings" => 'users#settings', as: "user_settings"
  root to: "home#index"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
