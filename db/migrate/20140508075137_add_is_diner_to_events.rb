class AddIsDinerToEvents < ActiveRecord::Migration
  def change
    add_column :events, :is_diner, :boolean, :default => false
  end
end
