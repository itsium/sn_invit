class AddIsDinerGuestToReplies < ActiveRecord::Migration
  def change
    add_column :replies, :is_diner_guest, :boolean
  end
end
