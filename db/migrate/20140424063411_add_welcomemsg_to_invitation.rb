class AddWelcomemsgToInvitation < ActiveRecord::Migration
  def change
    add_column :invitations, :welcomemsg, :string
  end
end
