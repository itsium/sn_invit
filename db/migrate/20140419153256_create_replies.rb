class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.string :name
      t.string :nbpeople
      t.string :phone
      t.string :message
      t.references :invitation, index: true

      t.timestamps
    end
  end
end
