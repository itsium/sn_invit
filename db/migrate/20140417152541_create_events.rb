class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :address
      t.float :longitude
      t.float :latitude
      t.datetime :eventtime
      t.string :phone
      t.references :invitation, index: true

      t.timestamps
    end
  end
end
