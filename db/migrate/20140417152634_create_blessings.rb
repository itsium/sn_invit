class CreateBlessings < ActiveRecord::Migration
  def change
    create_table :blessings do |t|
      t.text :content
      t.string :name
      t.references :invitation, index: true

      t.timestamps
    end
  end
end
