class AddAttachmentBgmusicToInvitations < ActiveRecord::Migration
  def self.up
    change_table :invitations do |t|
      t.attachment :bgmusic
    end
  end

  def self.down
    drop_attached_file :invitations, :bgmusic
  end
end
