class AddInvitationIdToAttachement < ActiveRecord::Migration
  def change
    add_reference :attachements, :invitation, index: true
  end
end
