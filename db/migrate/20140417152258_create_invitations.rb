class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :bride
      t.string :groom
      t.date :weddingday
      t.string :theme
      t.references :user, index: true

      t.timestamps
    end
  end
end
