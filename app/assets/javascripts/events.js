//= require bootstrap-datetimepicker.min
//= require jquery.validate.min

jQuery(function($) {
  $('#eventtimeinput').datetimepicker();

 $("#new_event").validate({
     rules: {
      event_name: "required",
      event_address: "required",
      event_longitude: "required",
      event_latitude: "required",
      event_phone: "required",
      eventtimeinput: "required"
    }
  });

});

