// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery-1.11.0.min
//= require jquery_ujs
//= require jquery.mobile.custom.min
//= require bootstrap.min
//= require typeahead-bs2.min
//= require jquery.colorbox-min
//= require ace-elements.min
//= require ace.min
//= require bootbox.min
//= require ace-extra.min
//= require_tree .
//= stub active_admin


jQuery(function($) {
  $("#new_app_btn").on(ace.click_event, function() {
    bootbox.prompt("What is your app's name?", function(result) {
      if (result === null) {
        //Example.show("Prompt dismissed");
      } else {
        //Example.show("Hi <b>"+result+"</b>");
        $.post( "/invitations", { authenticity_token: $('meta[name="csrf-token"]').attr('content'), invitation: {user_id: cur_uid ,name: result} } );

      }
    });
  });


//   var some_html = '<img src="images/bootstrap_logo.png" width="100px"/><br />';
// some_html += '<h2>You can use custom HTML too!</h2><br />';
// some_html += '<h4>Just be sure to mind your quote marks</h4>';
// bootbox.alert();
  $("#new_photo_btn").on(ace.click_event, function() {


    bootbox.dialog({
      message: "<button>aaa</button>",
      title: "Custom title",
      buttons: {
        success: {
          label: "Success!",
          className: "btn-success",
          callback: function() {
            // Example.show("great success");
          }
        }
      }
      });
  });

  $('#gallery_new_img_input').ace_file_input({
          style:'well',
          btn_choose:"Drop images here or click to choose",
          btn_change:null,
          no_icon:'icon-picture',
          droppable:true,
          thumbnail:'small',//large | fit
          //,icon_remove:null//set null, to hide remove/reset button
         before_change:function(files, dropped) {
            //Check an example below
            //or examples/file-upload.html
            var allowed_files = [];
              for(var i = 0 ; i < files.length; i++) {
                var file = files[i];
                if(typeof file === "string") {
                  //IE8 and browsers that don't support File Object
                  if(! (/\.(jpe?g|png|gif|bmp)$/i).test(file) ) return false;
                }
                else {
                  var type = $.trim(file.type);
                  if( ( type.length > 0 && ! (/^image\/(jpe?g|png|gif|bmp)$/i).test(type) )
                      || ( type.length == 0 && ! (/\.(jpe?g|png|gif|bmp)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
                    ) continue;//not an image so don't keep this file
                }

                allowed_files.push(file);
              }
              if(allowed_files.length == 0) return false;

            return allowed_files;
          }
          /**,before_remove : function() {
            return true;
          }*/
          ,
          preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
          }

        }).on('change', function(){
          //console.log($(this).data('ace_input_files'));
          //console.log($(this).data('ace_input_method'));
    });

    var colorbox_params = {
    reposition:true,
    scalePhotos:true,
    scrolling:false,
    previous:'<i class="icon-arrow-left"></i>',
    next:'<i class="icon-arrow-right"></i>',
    close:'&times;',
    current:'{current} of {total}',
    maxWidth:'100%',
    maxHeight:'100%',
    onOpen:function(){
      document.body.style.overflow = 'hidden';
    },
    onClosed:function(){
      document.body.style.overflow = 'auto';
    },
    onComplete:function(){
      $.colorbox.resize();
    }
  };

  $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
  $("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon

  /**$(window).on('resize.colorbox', function() {
    try {
      //this function has been changed in recent versions of colorbox, so it won't work
      $.fn.colorbox.load();//to redraw the current frame
    } catch(e){}
  });*/



});
