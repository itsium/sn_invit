# == Schema Information
#
# Table name: blessings
#
#  id            :integer          not null, primary key
#  content       :text
#  name          :string(255)
#  invitation_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Blessing < ActiveRecord::Base
  belongs_to :invitation
end
