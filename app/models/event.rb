# == Schema Information
#
# Table name: events
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  address       :text
#  longitude     :float
#  latitude      :float
#  eventtime     :datetime
#  phone         :string(255)
#  invitation_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Event < ActiveRecord::Base
  belongs_to :invitation
  validates :name, :address, :longitude, :latitude, :eventtime, presence: true

  def as_json(options=nil)
    item = super({ only: [
        :id, :name, :address, :longitude, :latitude, :eventtime, :phone
    ] }.merge(options || {}))
    item.merge({
      eventmoment: self.eventtime.strftime("%m月%d日 %H:%M")
    })
  end
end
