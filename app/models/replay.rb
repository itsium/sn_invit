# == Schema Information
#
# Table Replies
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  nbpeople      :integer
#  phone         :string(255)
#  message       :string(255)
#  invitation_id :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Reply < ActiveRecord::Base
  belongs_to :invitation
end
