# == Schema Information
#
# Table name: attachements
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  invitation_id      :integer
#

class Attachement < ActiveRecord::Base
  belongs_to :invitation
  has_attached_file :image, :styles => { :original => "1024x1024>", :thumb => "150x150#" }, :default_url => "/assets/image_missing_:style.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  def as_json(options=nil)
    item = super({ only: [:id] }.merge(options || {}))
    item.merge({
        image: self.image.url(:original),
        thumb: self.image.url(:thumb)
    })
  end
end
