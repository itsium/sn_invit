# == Schema Information
#
# Table name: invitations
#
#  id                   :integer          not null, primary key
#  bride                :string(255)
#  groom                :string(255)
#  weddingday           :date
#  theme                :string(255)
#  user_id              :integer
#  created_at           :datetime
#  updated_at           :datetime
#  name                 :string(255)
#  welcomemsg           :string(255)
#  bgmusic_file_name    :string(255)
#  bgmusic_content_type :string(255)
#  bgmusic_file_size    :integer
#  bgmusic_updated_at   :datetime
#

class Invitation < ActiveRecord::Base
  belongs_to :user
  validates :name, :presence => true
  has_many :replies
  has_many :blessings
  has_many :attachements
  has_many :events
  has_attached_file :bgmusic

  #validates_attachment_presence :bgmusic
  validates_attachment_content_type :bgmusic, :content_type => [ 'application/mp3', 'application/x-mp3', 'audio/mpeg', 'audio/mp3', 'application/octet-stream' ],
                                    :message => 'file must be of filetype .mp3'
  validates_attachment_size :bgmusic, :less_than => 5.megabytes

  def as_json(options=nil)
    item = super({ only: [:groom, :bride, :weddingday, :welcomemsg, :theme] }.merge(options || {}))
    item.merge({
      music: self.bgmusic.url(),
      ldate: I18n.l(self.weddingday),
      subdate: I18n.l(self.weddingday, :format => '%A')
    })
  end
end
