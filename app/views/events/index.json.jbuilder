json.array!(@events) do |event|
  json.extract! event, :id, :name, :address, :longitude, :latitude, :eventtime, :phone, :invitation_id
  json.url event_url(event, format: :json)
end
