json.array!(@blessings) do |blessing|
  json.extract! blessing, :id, :content, :name, :invitation_id
  json.url blessing_url(blessing, format: :json)
end
