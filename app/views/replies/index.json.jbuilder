json.array!(@replies) do |reply|
  json.extract! reply, :id, :name, :nbpeople, :phone, :message, :invitation_id
  json.url reply_url(reply, format: :json)
end
