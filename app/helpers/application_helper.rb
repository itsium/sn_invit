module ApplicationHelper
   def flash_class(level)
      case level
        when :notice then "info"
        when :error then "danger"
        when :alert then "warning"
        else
          level
      end
    end
end
