class BlessingsController < InheritedResources::Base
  before_filter :authenticate_user!, except: [:app_list, :app_add]
  before_filter :sandbox_user_using_invitataion_id, only: [:index]
  def index
    @blessings = @invitation.blessings
  end

  def app_list
    skip = params[:start] ? params[:start].to_i : 0
    limit = params[:limit] ? params[:limit].to_i : 10

    @invitation = Invitation.find(params[:invitation_id])
    @blessings = @invitation.blessings
    @total = @blessings.count()
    @blessings = @blessings.limit(limit).offset(skip).order('created_at DESC')
  end

  def app_add
    @invitation = Invitation.find(params[:invitation_id])
    @blessing = @invitation.blessings.new(
      name: params[:name],
      content: params[:content]
    )
    @success = @blessing.save
  end

  private

    def permitted_params
      params.permit!
    end
end
