class RepliesController < InheritedResources::Base
  before_filter :authenticate_user!, except: [:app_add]
  before_filter :sandbox_user_using_invitataion_id, only: [:index]
  def index
    @diner_only_replies = @invitation.replies.where(is_diner_guest: true)
    @replies = @invitation.replies(is_diner_guest: false)
  end

  def app_add
    @invitation = Invitation.find(params[:invitation_id])
    @blessing = @invitation.replies.new(
      name: params[:name],
      nbpeople: params[:nbpeople].to_i,
      phone: params[:phone],
      message: params[:message],
      is_diner_guest: params[:is_diner_guest]
    )
    @success = @blessing.save
  end

  private

    def permitted_params
      params.permit!
    end
end
