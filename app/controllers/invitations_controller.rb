class InvitationsController < InheritedResources::Base
  # require 'rqrcode'
  before_filter :authenticate_user!
  before_filter :sandbox_user_using_invitataion_id, except: [:create, :new, :destroy_attachement]
  before_filter :gen_qrcode, only: [:show]

  def galery
    @photo = Attachement.new
    @photos = @invitation.attachements
  end

  def add_attachement
    @attachement = @invitation.attachements.new(params.require(:attachement).permit(:image, :name))
    if @attachement.save
      redirect_to :back, flash: { success: t(:photo_created) }
    else
      redirect_to :back, flash: { error: t(:photo_create_error) }
    end
  end

  def destroy_attachement
    @attachement = Attachement.find(params[:id])
    if @attachement.invitation.user_id == current_user.id
      @attachement.destroy
      redirect_to :back
    else
      redirect_to root_url, flash: {error: "Please don't cross user sandbox."}
    end
  end

private
  def gen_qrcode
    ui_path = Rails.env.development? ? ("roc_dist/") : ("i/")
    url =  "http://#{request.host}:#{request.port}/#{ui_path}?e=f"
    logger.debug "[DEBUG] ZZZZZZZZZZZZZZZZZZZZZZZZZ  #{url}"
    @fullqr = RQRCode::QRCode.new(url, :size => 5, :level => :h )
    @diner_only = RQRCode::QRCode.new( "http://#{request.host}:#{request.port}/#{ui_path}", :size => 5, :level => :h )
  end

  def permitted_params

    params.permit!
  end
end