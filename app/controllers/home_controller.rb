class HomeController < ApplicationController
  before_filter :authenticate_user!, except: [:app, :music]
  #before_filter :sandbox_user_using_invitataion_id, except: [:app]
  def index
    redirect_to user_path if current_user
  end

  def app
    # recuperer toutes les infos necessaire au lancement de l'app
    # params[:event_type] = f ou s
    @invitation = Invitation.find(params[:invitation_id])
    if params[:event_type]  &&  params[:event_type] == 'f'
      @events = @invitation.events.order(eventtime: :asc)
    else
      @events = @invitation.events.order(eventtime: :asc).where(is_diner: true)
    end
  end

  def music
    @invitation = Invitation.find(params[:invitation_id])
    send_file @invitation.bgmusic.path, disposition: "inline", x_sendfile: true
    #send_data File.read(@invitation.bgmusic.path), type: "audio/mpeg", disposition: "inline"
  end
end
