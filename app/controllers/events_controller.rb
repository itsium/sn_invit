class EventsController < InheritedResources::Base
  before_filter :authenticate_user!, except: [:app_list]
  before_filter :sandbox_user_using_invitataion_id, except: [:create, :new, :destroy, :destroy_event]
  before_filter :verifyinvitationid, only: [:create]

  def index
    @events = @invitation.events
    @event = Event.new
  end

  def create
    super do |format|
      format.html { redirect_to :back}
    end
  end

  def destroy_event
    destroy
  end

  def destroy
    super do |format|
      format.html { redirect_to :back}
    end
  end

  private
  def permitted_params
    params.permit!
  end

  def verifyinvitationid
    inv = Invitation.find(params[:event][:invitation_id])
    if inv.user_id != current_user.id
      redirect_to root_url, flash: {error: "Please don't cross user sandbox."}
    end
  end
end
